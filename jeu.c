#include "jeu.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int comparaison(char joueur, char ordinateur) {
   
    if (joueur == ordinateur) {
        return 0;
    } else if ((joueur == 'R' && ordinateur == 'C') || 
               (joueur == 'P' && ordinateur == 'R') || 
               (joueur == 'C' && ordinateur == 'P')) {
        return 1;
    } else {
        return 2;
}
}
char hazard() {
 
    int nombreAleatoire = rand() % 3;
    if (nombreAleatoire == 0) {
        return 'R';
    } else if (nombreAleatoire == 1) {
        return 'P';
    } else {
        return 'C';
    }
}
