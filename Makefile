CC=gcc
CFLAGS=-std=c99 -Wall

# additional flags for gcov
TESTFLAGS=-fprofile-arcs -ftest-coverage


app: jeu.h jeu.c app.c
	# build the app
	$(CC) $(CFLAGS) -o app app.c jeu.c

	# run the app
	./app


testintegration: testIntegration.c jeu.h jeu.c
	# build the integration test
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testIntegration testIntegration.c jeu.c

	# run the integration test
	./testIntegration P R R P C C P R

	# compute how test is covering testintegration.c
	gcov -c -p testIntegration

testhasard: testHasard.c jeu.h jeu.c
	# build the hasard test
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testHasard testHasard.c jeu.c

	# run the hasard test, which will generate testhasard.gcna and testhasard.gcno
	./testHasard

	# compute how test is covering testhasard.c
	gcov -c -p testHasard

testcomparaison:testComparaison.c jeu.h jeu.c
	# build the test
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testComparaison testComparaison.c jeu.c

	# run the test, which will generate testcomparaison.gcna and testComparaison.gcno
	./testComparaison

	# compute how test is covering testComparaison.c
	gcov -c -p testComparaison

clean:
	rm -f *.o app testIntegration testHasard testComparaison app *.gcov *.gcda *.gcno


